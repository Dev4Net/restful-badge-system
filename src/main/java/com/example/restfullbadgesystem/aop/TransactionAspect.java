package com.example.restfullbadgesystem.aop;

import com.example.restfullbadgesystem.domain.*;
import com.example.restfullbadgesystem.dto.CheckInDTO;
import com.example.restfullbadgesystem.service.LocationService;
import com.example.restfullbadgesystem.service.TransactionQueuePublisherService;
import com.example.restfullbadgesystem.services.BadgeService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

@Aspect
@Component
public class TransactionAspect {
    @Autowired
    private TransactionQueuePublisherService publisher;

    @Autowired
    private BadgeService badgeService;

    @Autowired
    private LocationService locationService;

    @After("execution(* com.example.restfullbadgesystem.controllers.CheckInController.checkIn(..)) && args(checkin)")
    public void getCheckInTransactions(JoinPoint joinPoint, CheckInDTO checkin){
        Transaction transaction = new Transaction();
        Badge badge = badgeService.getBadge(checkin.badgeId);
        Location location = locationService.getLocation(checkin.locationId);
        transaction.setMember(badge.getMember());
        transaction.setLocation(location);
        transaction.setType(TransactionType.CHECKIN);
        System.out.println("Inside AOP");
        transaction.setDateTime(Date.from(Instant.now()));
        publisher.send(transaction);
    }

//    @After("execution(* *.*.* (..))")
//    public void test(){
//        System.out.println("check AOP");
//    }
}
