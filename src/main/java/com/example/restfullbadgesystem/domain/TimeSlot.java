package com.example.restfullbadgesystem.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class TimeSlot {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss.SSSZ")
    private Date startTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss.SSSZ")
    private Date endTime;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    private Collection<DayOfWeek> daysOfWeek; // enum DayOfWeek is already available since Java 1.8
    
    public TimeSlot(Date startTime, Date endTime, Collection<DayOfWeek> daysOfWeek) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.daysOfWeek = daysOfWeek;
	}
}
