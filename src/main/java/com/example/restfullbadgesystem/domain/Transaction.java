package com.example.restfullbadgesystem.domain;

import com.fasterxml.jackson.annotation.JacksonAnnotation;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


//    private String dateTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date dateTime;
    @ManyToOne
    private Member member;

    @ManyToOne
    private Location location;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    public Transaction(Date dateTimeString, Member member, Location location, TransactionType type) {
        this.dateTime = dateTimeString;
        this.member = member;
        this.location = location;
        this.type = type;
    }
}
