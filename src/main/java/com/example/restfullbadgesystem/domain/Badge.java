package com.example.restfullbadgesystem.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Data
public class Badge {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date issueDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date expireDate;

    private Boolean isActive;
  
    @ManyToOne
    private Member member;
  
    public Badge() {
    }

    public Badge(Member member) {
        this.isActive = true;
        this.member = member;
        this.issueDate = Date.from(Instant.now());
        if (member.getRoles().contains("STAFF")) {
            this.expireDate.setMonth(issueDate.getMonth() + 12);
        } else if (member.getRoles().contains("FACULTY")) {
            this.expireDate.setMonth(issueDate.getMonth() + 6);
        }   else { this.expireDate.setMonth(issueDate.getMonth() + 8);
        }
    }

}
