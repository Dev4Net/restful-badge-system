package com.example.restfullbadgesystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy
@SpringBootApplication
public class RestfullBadgeSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestfullBadgeSystemApplication.class, args);
    }
}
